import socket
from flask import Flask

app = Flask(__name__)

# Get env variable TAG
hostname = socket.gethostname()

@app.route('/')
def hello():
    return f'Máquina: {hostname}'